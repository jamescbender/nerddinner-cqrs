﻿using System;
using System.Configuration;

using CommonDomain;
using CommonDomain.Core;
using CommonDomain.Persistence;
using CommonDomain.Persistence.EventStore;

using EventStore;

using MassTransit;

using Microsoft.Practices.Unity;

using NerdDinner.AppService.Consumers;

namespace NerdDinner.AppService
{
    public class AppService : IDisposable
    {

        #region Fields

        private IStoreEvents _Storage;
        public static IServiceBus _Bus;
        private string SubscriptionUri;
        private UnityContainer _Container;

        #endregion
        
        public AppService()
        {
            SubscriptionUri = "msmq://localhost/NerdDinner.SubscriptionService";
        }

        public void Initialize()
        {

            // EventStore
            try
            {
                _Storage = Wireup.Init()
                    .UsingSqlPersistence("NerdDinnerSQL")
                    .InitializeStorageEngine()
                    .UsingJsonSerialization()
                    .LogToConsoleWindow()
                    .UsingAsynchronousDispatchScheduler(new MassTransitDispatcher())
                    .Build();
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Error while configuring EventStore: " + e);
                Console.ReadLine();
            }
            // Unity
            _Container = new UnityContainer();
            _Container.RegisterInstance(typeof (IStoreEvents), _Storage, new ExternallyControlledLifetimeManager());
            _Container.RegisterType<IRepository, EventStoreRepository>();
            _Container.RegisterType<IDetectConflicts, ConflictDetector>();
            _Container.RegisterType<IConstructAggregates, AggregateFactory>();

            _Container.RegisterType<DinnerConsumer, DinnerConsumer>();

            // MassTransit
            var mtQueue = ConfigurationManager.AppSettings["MtQueue"];
            try
            {
                _Bus = ServiceBusFactory.New(sbc =>
                                                 {
                                                     sbc.UseMsmq();
                                                     sbc.VerifyMsmqConfiguration();
                                                     sbc.UseSubscriptionService(SubscriptionUri);
                                                     sbc.ReceiveFrom(mtQueue);
                                                     sbc.UseControlBus();
                                                     sbc.Subscribe(s =>s.LoadFrom(_Container));
                                                 });
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("Error while configuring MT: {0}", ex);
                throw;
            }

        }

        public void Dispose()
        {
            Bus.Shutdown();
        }
    }
}