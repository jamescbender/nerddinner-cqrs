﻿using System;
using System.Configuration;

using MassTransit;

using Microsoft.Practices.Unity;

using NerdDinner.Denormalizer.Consumers;

namespace NerdDinner.Denormalizer
{
    public class AppService : IDisposable
    {

        #region Fields

        private string SubscriptionUri;
        private UnityContainer _Container;
        private IServiceBus _Bus;

        #endregion
        
        public AppService()
        {
            SubscriptionUri = "msmq://localhost/NerdDinner.SubscriptionService";
        }

        public void Start()
        {
            // Unity
            _Container = new UnityContainer();

            // Register consumers
            _Container.RegisterType<MongoUpdater, MongoUpdater>();
            _Container.RegisterType<DinnerEventConsumer, DinnerEventConsumer>();
            _Container.RegisterType<DinnerDetailConsumer, DinnerDetailConsumer>();

            // MassTransit
            var mtQueue = ConfigurationManager.AppSettings["MtQueue"];

            try
            {
                _Bus = ServiceBusFactory.New(sbc =>
                                                 {
                                                     sbc.UseMsmq();
                                                     sbc.VerifyMsmqConfiguration();
                                                     sbc.UseSubscriptionService(SubscriptionUri);
                                                     sbc.ReceiveFrom(mtQueue);
                                                     sbc.UseControlBus();
                                                     sbc.Subscribe(s => s.LoadFrom(_Container));
                                                 });
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Error while configuring MT: {0}", e);
                Console.ReadLine();
                throw;
            }

        }

        public void Dispose()
        {
            Bus.Shutdown();
            _Container.Dispose();
        }

    }
}