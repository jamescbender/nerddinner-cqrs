﻿using System;

using NerdDinner.Messages;
using NerdDinner.ReadModel;

namespace NerdDinner.Denormalizer.Consumers
{
    internal abstract class BaseConsumer
    {

        protected BaseConsumer(MongoUpdater upd)
        {
            Updater = upd;
        }

        protected MongoUpdater Updater { get; private set; }


        /// <summary>
        /// Return a new object to load with properties from Events
        /// </summary>
        /// <returns></returns>
        internal abstract IStoredInMongo NewItem();

        /// <summary>
        /// Can this consumer handle the event submitted?
        /// </summary>
        /// <param name="evt"></param>
        /// <returns></returns>
        internal abstract bool CanHandle(IEvent evt);

        internal abstract void Consume(IStoredInMongo obj, IEvent evt);

        /// <summary>
        /// Save the object back to the MongoStore
        /// </summary>
        /// <param name="obj"></param>
        internal void Save(IStoredInMongo obj)
        {
            Updater.Add(obj);
        }

    }
}