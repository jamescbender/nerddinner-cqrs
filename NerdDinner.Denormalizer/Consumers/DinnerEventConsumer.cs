﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MassTransit;

using NerdDinner.Messages.Events;
using NerdDinner.ReadModel;

namespace NerdDinner.Denormalizer.Consumers
{

    public class DinnerEventConsumer : Consumes<DinnerCreated>.All , Consumes<DinnerRescheduled>.All
    {

        #region Fields

        public MongoUpdater _Updater;

        #endregion
        
        public DinnerEventConsumer(MongoUpdater updater)
        {
            _Updater = updater;
        }


        public void Consume(DinnerCreated msg)
        {

            var d = new DinnerListItem()
                        {
                            Title = msg.Title,
                            EventDate = msg.EventDate.Date,
                            Description = msg.Description,
                            RsvpCount = 1,
                            Latitude = msg.Latitude,
                            Longitude = msg.Longitude,
                            Id = msg.AggregateId.ToString()
                        };

            _Updater.Add(d);

            Console.Out.WriteLine("Wrote DinnerCreated: {0}", msg.Title);

        }

        public void Consume(DinnerRescheduled msg)
        {

            var d = _Updater.GetById<DinnerListItem>(msg.AggregateId);

            d.EventDate = msg.NewEventTime;

            _Updater.Update(d);

            Console.Out.WriteLine("Rescheduled Dinner: {0}", msg.NewEventTime);

        }

        public bool CanHandle(object body)
        {
            return _Updater.CanHandle(body);
        }

        public DinnerListItem Consume(object p, DinnerListItem sa)
        {

            if (p is DinnerCreated)
            {
                var msg = p as DinnerCreated;
                var d = new DinnerListItem()
                {
                    Title = msg.Title,
                    EventDate = msg.EventDate.Date,
                    Description = msg.Description,
                    RsvpCount = 1,
                    Latitude = msg.Latitude,
                    Longitude = msg.Longitude,
                    Id = msg.AggregateId.ToString()
                };
                return d;
            }

            if (p is DinnerRescheduled)
            {
                var msg = p as DinnerRescheduled;
                sa.EventDate = msg.NewEventTime;
                return sa;
            }

            return sa;

        }

        public void Save(DinnerListItem sa)
        {
            _Updater.Add(sa);
        }
    }
}
