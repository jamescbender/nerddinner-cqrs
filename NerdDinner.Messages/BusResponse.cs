﻿using System;

using MassTransit;

namespace NerdDinner.Messages
{
 
    /// <summary>
    /// A wrapper to encapsulate response information for a synchronous call
    /// </summary>
    public class BusResponse : CorrelatedBy<Guid>
    {

        public bool Success;
        public Exception Exception;
        public string Message;

        public Guid CorrelationId { get; set; }

    }
}