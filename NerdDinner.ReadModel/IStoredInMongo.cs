﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NerdDinner.ReadModel
{

    public interface IStoredInMongo
    {

        string Id { get; }

        /// <summary>
        /// Name of the collection for this object
        /// </summary>
        string CollectionName { get; }

    }
}
