﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using EventStore;

namespace NerdDinner.Controllers
{
    public class AuditController : Controller
    {
        private IStoreEvents _Store;

        public AuditController()
        {
            if (_Store == null)
            {

                _Store = Wireup.Init()
                .UsingSqlPersistence("NerdDinnerSQL")
                .InitializeStorageEngine()
                .UsingJsonSerialization()
                    //.UsingBinarySerialization()
                //.Compress()
                .Build();

            }
        }

        //
        // GET: /Audit/

        public ActionResult Index(Guid id)
        {

            var events = _Store.OpenStream(id, 0, int.MaxValue);

            return View(events.CommittedEvents.ToArray());

        }

    }
}
