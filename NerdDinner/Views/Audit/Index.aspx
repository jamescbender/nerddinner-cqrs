﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ICollection<EventStore.EventMessage>>" %>
<%@ Import Namespace="NerdDinner.Messages" %>

<asp:Content ID="Title" ContentPlaceHolderID="TitleContent" runat="server">
    NerdDinner
</asp:Content>

<asp:Content ID="Masthead" ContentPlaceHolderID="MastheadContent" runat="server">
	<% Html.RenderPartial("Masthead", true); %>
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">

<h2>Dinner Audit</h2>

<table style="width: 500px;">
    <tr>
        <td><b>Date</b></td>
        <td><b>Change</b></td>
    </tr>
<%foreach (var msg in Model)
  {
      IEvent e = msg.Body as IEvent;
      %><tr>
            <td ><%= e.ActingDateTimeUtc.ToLocalTime() %></td>
            <td><%= e.ToString() %></td>
        </tr>
      <%
  } %>
</table>

</asp:Content>
